<!DOCTYPE html>
<html>
<head>
<meta charset= "utf-8">
</head>
<h1> Pet Shop - Grupo Javaliz Honorários </h1>
<h2>Updates do projeto: <mark>v.Final</mark> </h2>

<body>
<h3>1.0 - Autenticação do Sistema:<h3>
    <p>Autenticação é um processo que busca verificar a identidade digital do usuário de um sistema - <mark>Concluído</mark></p>
    <br>
<h3>2.0 - Menu Principal:<h3>
    <p>O colaborador do PetShop irá selecionar qual a operação irá realizar - <mark>Concuído</mark> </p>
<br><br>
<ul>

<li><b> 2.1 - Acesso Salão :</b> Mostram os serviços oferecidos (Banho e/ou tosa) - <mark>Concluído</mark></li>
    <p>Serão oferecidos os serviços de banho e/ou tosa</p>
    <br><br>

<li><b> 2.2 - Acesso Pet Loja :</b>Venda de produtos - <mark>Concluído (Etapas de conclusão: 100/100)</mark></li>
    <p>Elaboracão de estoque para cada produto oferecido pela loja - Alimentação, Higiene, Vestuário, Lazer e Medicações</p>
<p><b>------> Itens de Alimentos:</b> Os argumentos utilizados serão dividos por ENUM(Peso, idade, tipo e tamanho), sendo que para: Tamanho     (Pequeno, Médio, Grande e Gigante) e Peso(caquético, magro, normal, sobrepeso e obeso)<mark>Concluído</mark></p>
<p><b>------> Itens de Vestuário (total= 36 produtos):</b> Implementação de um array-list para cada item de Vestuário: Coleiras, Roupas e Presilhas.<mark>Concluído</mark></p>
<p><b>------> Itens de Higiene:</b>Serão implementados ENUM para os itens: Shampoo, sabão e escovas e Implementado outro ENUM para os tipos de pele: Normal ou Sensível. <mark>Concluído</mark></p>
<p><b>------> Itens de Medicações:</b> Serão implementados no ENUM (tipos): Antibiótico, Anti-Viral, Anti-Inflamatório e Vermífugo. (<i>double</i> gramagem: 0,25g, 0,50g, 0,75g, 1,00g) <mark>Concluído</mark></p>
<p><b>Itens de Lazer:</b> Será implementado um ENUMs para tipo: Habitação, cama e brinquedo. E outro ENUM para relcacionar de acordo com o tamanho do PET. <mark>Concluído</mark></p>

<br>
<li><b> 2.3 - Acesso Veterinário :</b>Clínica Veterinária 24hs - <mark>Concluído</mark></li>
   
<br>
</ul>

<h3> 3.0 - Cadastro de Cliente e PET - <mark>Concluído</mark><h3>
    <p>Nesta etapa, o colaborador irá cadastrar o cliente e PET. Todas as informações do pet estarão vinculadas ao Cliente(principal)</p>    


</body>
</html>


