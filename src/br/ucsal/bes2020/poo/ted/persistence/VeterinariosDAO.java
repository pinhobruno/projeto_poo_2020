package br.ucsal.bes2020.poo.ted.persistence;

import java.util.List;


import br.ucsal.bes2020.poo.ted.domain.Veterinario;
import java.util.ArrayList;


public class VeterinariosDAO {
	private static List<Veterinario> list = new ArrayList<>();
	
	
	public static void addVeterinario(Veterinario veterinario) {
		list.add(veterinario);
	}

	public static List<Veterinario> getList() {
		return list;
	}

	
}
