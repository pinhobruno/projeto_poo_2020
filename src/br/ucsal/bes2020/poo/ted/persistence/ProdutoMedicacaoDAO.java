package br.ucsal.bes2020.poo.ted.persistence;

import java.util.ArrayList;
import java.util.List;
import br.ucsal.bes2020.poo.ted.domain.ProdutoMedicacao;
import br.ucsal.bes2020.poo.ted.enums.TiposMedicacoes;

public class ProdutoMedicacaoDAO {

	private static List<ProdutoMedicacao> list = new ArrayList<>();
	public static void addEstoque() {

		list.add(new ProdutoMedicacao ("Antibiótico", 7.90, TiposMedicacoes.valueOf("ANTIBIOTICO"),  0.25));
		list.add(new ProdutoMedicacao ("Antibiótico", 12.90, TiposMedicacoes.valueOf("ANTIBIOTICO"), 0.50));
		list.add(new ProdutoMedicacao ("Antibiótico", 15.90, TiposMedicacoes.valueOf("ANTIBIOTICO"), 0.75));
		list.add(new ProdutoMedicacao ("Antibiótico", 17.90, TiposMedicacoes.valueOf("ANTIBIOTICO"), 1.00));

		// ----------------------------------------------------------------------------------------------------------

		list.add(new ProdutoMedicacao ("Anti-viral", 12.70, TiposMedicacoes.valueOf("ANTI-VIRAL"), 0.25));
		list.add(new ProdutoMedicacao ("Anti-viral", 14.80, TiposMedicacoes.valueOf("ANTI-VIRAL"), 0.50));
		list.add(new ProdutoMedicacao ("Anti-viral", 15.70, TiposMedicacoes.valueOf("ANTI-VIRAL"), 0.75));
		list.add(new ProdutoMedicacao ("Anti-viral", 16.90, TiposMedicacoes.valueOf("ANTI-VIRAL"), 1.00));

		//-----------------------------------------------------------------------------------------------------------

		list.add(new ProdutoMedicacao ("Anti-Inflamatório", 3.90, TiposMedicacoes.valueOf("ANTI-INFLAMATORIO"), 0.25));
		list.add(new ProdutoMedicacao ("Anti-Inflamatório", 7.90, TiposMedicacoes.valueOf("ANTI-INFLMATORIO"), 0.50));
		list.add(new ProdutoMedicacao ("Anti-Inflamatório", 9.90, TiposMedicacoes.valueOf("ANTI-INFLAMATORIO"), 0.75));
		list.add(new ProdutoMedicacao ("Anti-Inflamatório", 10.90, TiposMedicacoes.valueOf("ANTI-INFLAMATORIO"), 1.00));

		//-------------------------------------------------------------------------------------------------------------
		list.add(new ProdutoMedicacao ("Vermífugo", 7.90, TiposMedicacoes.valueOf("VERMIFUGO"), 0.25));
		list.add(new ProdutoMedicacao ("Vermífugo", 12.90, TiposMedicacoes.valueOf("VERMIFUGO"), 0.50));
		list.add(new ProdutoMedicacao ("Vermífugo", 15.90, TiposMedicacoes.valueOf("VERMIFUGO"), 0.75));
		list.add(new ProdutoMedicacao ("Vermífugo", 17.90, TiposMedicacoes.valueOf("VERMIFUGO"), 1.00));

		//--------------------------------------------------------------------------------------------------------------

	}
	public static List<ProdutoMedicacao> getList() {
		return list;
	}

	public static void listar() {
		for(ProdutoMedicacao x : list) {
			System.out.println(x);
		}
	}
}	
