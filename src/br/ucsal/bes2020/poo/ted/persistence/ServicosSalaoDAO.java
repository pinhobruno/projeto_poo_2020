package br.ucsal.bes2020.poo.ted.persistence;

import java.util.HashMap;
import java.util.Map;

import br.ucsal.bes2020.poo.ted.domain.Cliente;
import br.ucsal.bes2020.poo.ted.domain.ServicosSalao;

public class ServicosSalaoDAO {

	
	private static Map<Cliente,ServicosSalao> listaServicos = new HashMap<>();

	public static Map<Cliente, ServicosSalao> getListaServicos() {
		return listaServicos;
	}
	public static void add(Cliente cliente,ServicosSalao servicoSalao) {
		listaServicos.put(cliente, servicoSalao);
	}


}
