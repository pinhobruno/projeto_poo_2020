package br.ucsal.bes2020.poo.ted.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes2020.poo.ted.domain.ProdutoHigiene;
import br.ucsal.bes2020.poo.ted.domain.ProdutoLazer;
import br.ucsal.bes2020.poo.ted.enums.PorteAnimal;
import br.ucsal.bes2020.poo.ted.enums.TipoLazer;


public class ProdutoLazerDAO {
	private static List<ProdutoLazer> list = new ArrayList<>();

	public static void addEstoque() {
		
		
		
		list.add(new ProdutoLazer("Habitacao", 70.00, TipoLazer.valueOf("HABITACAO"), PorteAnimal.valueOf("PEQUENO")));
		list.add(new ProdutoLazer("Habitacao", 97.00, TipoLazer.valueOf("HABITACAO"), PorteAnimal.valueOf("MEDIO")));
		list.add(new ProdutoLazer("Habitacao", 137.00, TipoLazer.valueOf("HABITACAO"), PorteAnimal.valueOf("GRANDE")));
		list.add(new ProdutoLazer("Habitacao", 177.00, TipoLazer.valueOf("HABITACAO"), PorteAnimal.valueOf("GIGANTE")));
		// -------------------------------------------------------------------------------------------------------------------------
		list.add(new ProdutoLazer("Cama", 35.00, TipoLazer.valueOf("CAMA"), PorteAnimal.valueOf("PEQUENO")));
		list.add(new ProdutoLazer("Cama", 35.00, TipoLazer.valueOf("CAMA"), PorteAnimal.valueOf("MEDIO")));
		list.add(new ProdutoLazer("Cama", 35.00, TipoLazer.valueOf("CAMA"), PorteAnimal.valueOf("GRANDE")));
		list.add(new ProdutoLazer("Cama", 35.00, TipoLazer.valueOf("CAMA"), PorteAnimal.valueOf("GIGANTE")));
		// -------------------------------------------------------------------------------------------------------------------------
		list.add(new ProdutoLazer("Brinquedo", 35.00, TipoLazer.valueOf("BRINQUEDO"), PorteAnimal.valueOf("PEQUENO")));
		list.add(new ProdutoLazer("Brinquedo", 35.00, TipoLazer.valueOf("BRINQUEDO"), PorteAnimal.valueOf("MEDIO")));
		list.add(new ProdutoLazer("Brinquedo", 35.00, TipoLazer.valueOf("BRINQUEDO"), PorteAnimal.valueOf("GRANDE")));
		list.add(new ProdutoLazer("Brinquedo", 35.00, TipoLazer.valueOf("BRINQUEDO"), PorteAnimal.valueOf("GIGANTE")));
	}

	
	
	public static int totalEstoque() {
		int contador = 0;
		
		for(@SuppressWarnings("unused") ProdutoLazer e : list) {
			
			contador++;
			
		}
		
		return contador;
	}
	
	public static List<ProdutoLazer> getList() {
		return list;
	}

	public static void listar() {
		for (ProdutoLazer x : list) {
			System.out.println(x);
		}
	}
}