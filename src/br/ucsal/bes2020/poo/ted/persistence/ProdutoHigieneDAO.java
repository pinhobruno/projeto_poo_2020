package br.ucsal.bes2020.poo.ted.persistence;

import br.ucsal.bes2020.poo.ted.domain.ProdutoAlimento;
import br.ucsal.bes2020.poo.ted.domain.ProdutoHigiene;
import br.ucsal.bes2020.poo.ted.enums.TipoDePele;
import br.ucsal.bes2020.poo.ted.enums.TipoHigiene;

import java.util.ArrayList;
import java.util.List;

public class ProdutoHigieneDAO {


    private static List<ProdutoHigiene> list = new ArrayList<>();


    public static void addEstoque() {

        //350ML
        list.add(new ProdutoHigiene("Shampoo Porcao", 20.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("Shampoo Cadu", 20.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("Shampoo UX", 20.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("Shampoo MATA PIOLHO", 20.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("NORMAL")));
        //
        list.add(new ProdutoHigiene("Shampoo Xandog Plus", 32.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("Shampoo Pet Clean", 32.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("Shampoo Pet Smack", 32.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("Shampoo pet Sensibilidade", 32.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("SENSIVEL")));
        //3L
        list.add(new ProdutoHigiene("Shampoo Acua ", 120.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("Shampoo MisterM", 120.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("Shampoo TodoDia", 120.0, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("Shampoo Incrivel", 120.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("SENSIVEL")));

        list.add(new ProdutoHigiene("Shampoo Magnum ", 100.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("Shampoo Pera", 100.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("Shampoo Caju", 100.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("Shampoo Manga", 100.00, TipoHigiene.valueOf("SHAMPOO"), TipoDePele.valueOf("NORMAL")));
        //-------------------------------------------------------------------------------------------------------------------------------------------//
        list.add(new ProdutoHigiene("SABONETE Mata Piolho", 16.00, TipoHigiene.valueOf("SABAO"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("SOBONETE Mata Carrapato", 16.00, TipoHigiene.valueOf("SABAO"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("SABONETE Mata Pragas", 16.00, TipoHigiene.valueOf("SABAO"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("SOBONETE Mata Sarna", 16.00, TipoHigiene.valueOf("SABAO"), TipoDePele.valueOf("NORMAL")));

        list.add(new ProdutoHigiene("SOBONETE Sensibility", 25.00, TipoHigiene.valueOf("SABAO"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("SOBONETE NoSense", 25.00, TipoHigiene.valueOf("SABAO"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("SOBONETE SenseEight", 25.00, TipoHigiene.valueOf("SABAO"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("SOBONETE para pele sensivel G", 28.00, TipoHigiene.valueOf("SABAO"), TipoDePele.valueOf("SENSIVEL")));
        //-------------------------------------------------------------------------------------------------------------------------------------------//
        list.add(new ProdutoHigiene("Escova Cachinhos", 32.00, TipoHigiene.valueOf("ESCOVAS"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("Escova Dourada", 32.00, TipoHigiene.valueOf("ESCOVAS"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("Escova Serrilhada", 32.00, TipoHigiene.valueOf("ESCOVAS"), TipoDePele.valueOf("NORMAL")));
        list.add(new ProdutoHigiene("Escova Pigmentada", 32.00, TipoHigiene.valueOf("ESCOVAS"), TipoDePele.valueOf("NORMAL")));

        list.add(new ProdutoHigiene("Escova de Anubis", 28.00, TipoHigiene.valueOf("ESCOVAS"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("Escova de Iliao", 28.00, TipoHigiene.valueOf("ESCOVAS"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("Escova geral", 28.00, TipoHigiene.valueOf("ESCOVAS"), TipoDePele.valueOf("SENSIVEL")));
        list.add(new ProdutoHigiene("Esocva 7 em 1", 28.00, TipoHigiene.valueOf("ESCOVAS"), TipoDePele.valueOf("SENSIVEL")));
        
   
    }
/*public static void removerItemHigiene( ProdutoHigiene e) {
		
		list.remove(e);
		
	}*/
	
	public static int totalEstoque() {
		int contador = 0;
		
		for(@SuppressWarnings("unused") ProdutoHigiene e : list) {
			
			contador++;
			
		}
		
		return contador;
	}
    public static List<ProdutoHigiene> getList() {
        return list;
    }



    public static void listar() {
        for(ProdutoHigiene x : list) {
            System.out.println(x);
        }
    }

}
