package br.ucsal.bes2020.poo.ted.persistence;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import br.ucsal.bes2020.poo.ted.domain.ProdutoAlimento;
import br.ucsal.bes2020.poo.ted.enums.IdadeAnimal;
import br.ucsal.bes2020.poo.ted.enums.PesoAnimal;
import br.ucsal.bes2020.poo.ted.enums.PorteAnimal;
import br.ucsal.bes2020.poo.ted.enums.TipoDeAlimento;




public class ProdutoAlimentoDAO {

	private static List<ProdutoAlimento> list = new ArrayList<>();

	public static void addEstoque() {
		//(String nomeProdutos, double valorProdutos, PorteAnimal tamanho, IdadeAnimal idade, PesoAnimal peso,TipoDeAlimento alimento)

		//Filhote
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Ra��o para Filhote Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Ra��o para Filhote Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Ra��o para Filhote Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Ra��o para Filhote Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Filhote Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//Jovem
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Ra��o para Jovem Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Ra��o para Jovem Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Ra��o para Jovem Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Ra��o para Jovem Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Jovem Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//Adulto
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Ra��o para Adulto Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Ra��o para Adulto Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Ra��o para Adulto Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Ra��o para Adulto Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Adulto Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//Idoso
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Ra��o para Idoso Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Ra��o para Idoso Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Ra��o para Idoso Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Ra��o para Idoso Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("RACAO")));
		list.add(new ProdutoAlimento("Ra��o para Idoso Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("RACAO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		
		//Filhote
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Petisco para Filhote Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Petisco para Filhote Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Petisco para Filhote Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Petisco para Filhote Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Filhote Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//Jovem
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Petisco para Jovem Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Petisco para Jovem Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Petisco para Jovem Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Petisco para Jovem Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Jovem Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//Adulto
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Petisco para Adulto Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Petisco para Adulto Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Petisco para Adulto Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Petisco para Adulto Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Adulto Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//Idoso
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Petisco para Idoso Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Petisco para Idoso Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Petisco para Idoso Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Petisco para Idoso Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Petisco para Idoso Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


		
		//Filhote
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Osso para Filhote Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Osso para Filhote Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Osso para Filhote Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Osso para Filhote Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("PETISCO")));
		list.add(new ProdutoAlimento("Osso para Filhote Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("FILHOTE"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("PETISCO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//Jovem
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Osso para Jovem Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Osso para Jovem Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Osso para Jovem Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Osso para Jovem Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Jovem Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("JOVEM"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//Adulto
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Osso para Adulto Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Osso para Adulto Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Osso para Adulto Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Osso para Adulto Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Adulto Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("ADULTO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		//Idoso
		//Ra�a de Porte Pequeno
		list.add(new ProdutoAlimento("Osso para Idoso Desnutrido | Ra�a de Porte Pequeno ", 50.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Magro | Ra�a de Porte Pequeno", 45.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Normal | Ra�a de Porte Pequeno", 25.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Robusto | Ra�a de Porte Pequeno", 30.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Obeso | Ra�a de Porte Pequeno", 55.00, PorteAnimal.valueOf("PEQUENO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//Ra�a de Porte Medio
		list.add(new ProdutoAlimento("Osso para Idoso Desnutrido | Ra�a de Porte Medio ", 50.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Magro | Ra�a de Porte Medio ", 45.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Normal | Ra�a de Porte Medio ", 25.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Robusto | Ra�a de Porte Medio ", 30.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Obeso | Ra�a de Porte Medio ", 55.00, PorteAnimal.valueOf("MEDIO"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//Ra�a de Porte Grande
		list.add(new ProdutoAlimento("Osso para Idoso Desnutrido | Ra�a de Porte Grande ", 50.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("CAQUETICO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Magro | Ra�a de Porte Grande ", 45.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Normal | Ra�a de Porte Grande ", 25.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Robusto | Ra�a de Porte Grande ", 30.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Obeso | Ra�a de Porte Grande ", 55.00, PorteAnimal.valueOf("GRANDE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//Ra�a de Porte Gigante
		list.add(new ProdutoAlimento("Osso para Idoso Magro | Ra�a de Porte Gigante ", 45.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("MAGRO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Normal | Ra�a de Porte Gigante ", 25.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("NORMAL"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Robusto | Ra�a de Porte Gigante ", 30.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("SOBREPESO"),TipoDeAlimento.valueOf("OSSO")));
		list.add(new ProdutoAlimento("Osso para Idoso Obeso | Ra�a de Porte Gigante ", 55.00, PorteAnimal.valueOf("GIGANTE"),IdadeAnimal.valueOf("IDOSO"),PesoAnimal.valueOf("OBESO"),TipoDeAlimento.valueOf("OSSO")));
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	}
	
	public static void listarPorValor() {
		
		System.out.println("PRODUTOS ESTAO SENDO LISTADOS POR ORDEM DE VALOR !");

		list.sort(Comparator.comparing(ProdutoAlimento::getValorProdutos));
		
		// em ordem de valoresDosProdutos xD
	}
	
	
	/*public static void removerItemHigiene( ProdutoAlimento e) {
		
		list.remove(e);
		
	}*/
	
	public static int totalEstoque() {
		int contador = 0;
		
		for(@SuppressWarnings("unused") ProdutoAlimento e : list) {
			
			contador++;
			
		}
		
		return contador;
	}
	
	
	public static List<ProdutoAlimento> getList() {
		return list;
	}
	public static void listar() {
		for(ProdutoAlimento x : list) {
			System.out.println(x);
		}
	}
}
