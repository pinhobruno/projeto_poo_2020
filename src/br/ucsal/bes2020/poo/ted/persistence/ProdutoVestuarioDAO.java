package br.ucsal.bes2020.poo.ted.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes2020.poo.ted.domain.ProdutoVestuario;
import br.ucsal.bes2020.poo.ted.enums.TipoVestuario;

public class ProdutoVestuarioDAO {

	private static List<ProdutoVestuario> list = new ArrayList<>();

	
	public static void addEstoque() {

		list.add(new ProdutoVestuario("Coleira Verde", 20.00, "P", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Azul", 20.00, "P", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Amarelo", 20.00, "P", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Vermelho", 20.00, "P", TipoVestuario.valueOf("COLEIRA")));

		list.add(new ProdutoVestuario("Coleira Verde", 25.00, "M", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Azul", 25.00, "M", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Amarelo", 25.00, "M", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Vermelho", 25.00, "M", TipoVestuario.valueOf("COLEIRA")));

		list.add(new ProdutoVestuario("Coleira Verde", 30.00, "G", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Azul", 30.00, "G", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Amarelo", 30.00, "G", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Vermelho", 30.00, "G", TipoVestuario.valueOf("COLEIRA")));

		list.add(new ProdutoVestuario("Coleira Verde", 35.00, "GG", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Azul", 35.00, "GG", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Amarelo", 35.00, "GG", TipoVestuario.valueOf("COLEIRA")));
		list.add(new ProdutoVestuario("Coleira Vermelho", 35.00, "GG", TipoVestuario.valueOf("COLEIRA")));
		
		// -----------------------------------------------------------------------------------------------
		
		list.add(new ProdutoVestuario("Roupa Verde", 25.00, "P", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Azul", 25.00, "P", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Amarelo", 25.00, "P", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Vermelho",25.00, "P", TipoVestuario.valueOf("ROUPA")));

		list.add(new ProdutoVestuario("Roupa Verde", 30.00, "M", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Azul", 30.00, "M", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Amarelo", 30.00, "M", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Vermelho", 30.00, "M", TipoVestuario.valueOf("ROUPA")));

		list.add(new ProdutoVestuario("Roupa Verde", 35.00, "G", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Azul", 35.00, "G", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Amarelo", 35.00, "G", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Vermelho", 35.00, "G", TipoVestuario.valueOf("ROUPA")));

		list.add(new ProdutoVestuario("Roupa Verde", 40.00, "GG", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Azul", 40.00, "GG", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Amarelo", 40.00, "GG", TipoVestuario.valueOf("ROUPA")));
		list.add(new ProdutoVestuario("Roupa Vermelho", 40.00, "GG", TipoVestuario.valueOf("ROUPA")));
		// -----------------------------------------------------------------------------------------------
		
		list.add(new ProdutoVestuario("Presilha Verde", 10.00, "P", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Azul", 10.00, "P", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Amarelo", 10.00, "P", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Vermelho", 10.00, "P", TipoVestuario.valueOf("PRESILHA")));

		list.add(new ProdutoVestuario("Presilha Verde", 15.00, "M", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Azul", 15.00, "M", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Amarelo", 15.00, "M", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Vermelho", 15.00, "M", TipoVestuario.valueOf("PRESILHA")));

		list.add(new ProdutoVestuario("Presilha Verde", 20.00, "G", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Azul",20.00, "G", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Amarelo", 20.00, "G", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Vermelho", 20.00, "G", TipoVestuario.valueOf("PRESILHA")));

		list.add(new ProdutoVestuario("Presilha Verde", 25.00, "GG", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Azul", 25.00, "GG", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Amarelo", 25.00, "GG", TipoVestuario.valueOf("PRESILHA")));
		list.add(new ProdutoVestuario("Presilha Vermelho",25.00, "GG", TipoVestuario.valueOf("PRESILHA")));
		
		
		// -----------------------------------------------------------------------------------------------

	}

	public static List<ProdutoVestuario> getList() {
		return list;
	}
	
	

	public static void listar() {
		for(ProdutoVestuario x : list) {
			System.out.println(x);
		}
	}
	

	

}
