package br.ucsal.bes2020.poo.ted.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes2020.poo.ted.domain.Pet;

public class PetDAO {

	private static List<Pet> list = new ArrayList<>();

	public static void addPet(Pet pet) {
		list.add(pet);
	}

	public static List<Pet> getList() {
		return list;
	}
}
