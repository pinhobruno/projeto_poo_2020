package br.ucsal.bes2020.poo.ted.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes2020.poo.ted.domain.Cliente;

public class ClienteDAO {

	private static List<Cliente> list = new ArrayList<>();

	public static void addCliente(Cliente cliente) {
		list.add(cliente);
	}

	public static List<Cliente> getList() {
		return list;
	}

}
