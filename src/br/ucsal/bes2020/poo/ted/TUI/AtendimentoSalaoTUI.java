package br.ucsal.bes2020.poo.ted.TUI;

import java.util.Map.Entry;
import java.util.Scanner;

import br.ucsal.bes2020.poo.ted.domain.Cliente;
import br.ucsal.bes2020.poo.ted.domain.Pet;
import br.ucsal.bes2020.poo.ted.domain.ServicosSalao;
import br.ucsal.bes2020.poo.ted.enums.PorteAnimal;
import br.ucsal.bes2020.poo.ted.persistence.ClienteDAO;
import br.ucsal.bes2020.poo.ted.persistence.PetDAO;

import br.ucsal.bes2020.poo.ted.persistence.ServicosSalaoDAO;

public class AtendimentoSalaoTUI {
	
	public static void Cadastro() {
		int contador = 0;
		int escolha;
		do {
		if(contador > 0 ) {
			System.out.println("VAMOS PARA O "+(contador+1)+"� "+"CADASTRO NA LISTA DO SISTEMA");
		}
		Scanner sc = new Scanner(System.in);
		System.out.println();
		System.out.println("------------------------------------------");
		System.out.println("ENTRE COM OS DADOS DO CLIENTE: ");
		System.out.println("");
		System.out.println("NOME: ");
		String nome = sc.next();
		System.out.println("CPF: ");
		String cpf = sc.next();
		System.out.println("TELEFONE: ");
		String telefone = sc.next();
		System.out.println("E-MAIL: ");
		String email = sc.next();
		System.out.println("CEP: ");
		String cep = sc.next();
		System.out.println();
		Cliente cliente = new Cliente(nome, cpf, telefone, email, cep);
		ClienteDAO.addCliente(cliente);
		System.out.println();
		System.out.println("------------------------------------------");
		System.out.println("ENTRE COM OS DADOS DO PET: ");
		System.out.println("");
		System.out.println("NOME DO PET: ");
		String nomePet = sc.next();
		System.out.println("ESP�CIE: ");
		String especie = sc.next();
		System.out.println("RA�A: ");
		String raca = sc.next();
		System.out.println("TIPO DE PORTE DO ANIMAL : PEQUENO || MEDIO || GRANDE || GIGANTE");
		String tipoPorte = sc.next().toUpperCase();
		System.out.println();
		System.out.println("------------------------------------------");
		Pet pet = new  Pet(nomePet,especie,raca,PorteAnimal.valueOf(tipoPorte));
		PetDAO.addPet(pet);
		System.out.println("-------------------------------");
		System.out.println("BEM-VINDO(A) AO NOSSO SALAO !!");
		System.out.println("");
		System.out.println("-----SERVICO A SER ESCOLHIDO-----");
		System.out.println();
		System.out.println("***********************************");
		System.out.println("[1]- BANHO ");
		System.out.println("R$ 10.00 -- PEQUENO\nR$ 15.00 -- MEDIO\nR$ 20.00 -- GRANDE\nR$ 25.00 -- GIGANTE");
		System.out.println("***********************************");
		System.out.println("[2]- TOSA ");
		System.out.println("R$ 15.00 -- PEQUENO\nR$ 25.00 -- MEDIO\nR$ 30.00 -- GRANDE\nR$ 35.00 -- GIGANTE");
		System.out.println("***********************************");
		System.out.println("[3]- AMBOS ");
		System.out.println("R$ 25.00 -- PEQUENO\nR$ 40.00 -- MEDIO\nR$ 50.00 -- GRANDE\nR$ 60.00 -- GIGANTE");
		System.out.println("***********************************");
		System.out.println();
		System.out.println("O CLIENTE DESEJA O SERVICO DO TIPO : ");
		int x = sc.nextInt();
		
		//ServicosSalao servicosSalao = new ServicosSalao(pet,retorno);
		
		
		//if(x == 1 && tipoPorte.contentEquals("pequeno".toUpperCase()))
		
		if(x == 1 && tipoPorte.equalsIgnoreCase("pequeno")){	
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DO BANHO SERA DE R$ 10.00 "));
		}
		else if(x == 1 && tipoPorte.equalsIgnoreCase("medio")) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DO BANHO SERA DE R$ 15.00 "));
		}
		else if(x == 1 && tipoPorte.contentEquals("grande".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DO BANHO SERA DE R$ 20.00 "));
		}
		else if(x == 1 && tipoPorte.contentEquals("gigante".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DO BANHO SERA DE R$ 25.00 "));
		}
		else if(x == 2 && tipoPorte.contentEquals("pequeno".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DA TOSA SERA DE R$ 15.00 "));

		}
		else if(x == 2 && tipoPorte.contentEquals("medio".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DA TOSA SERA DE R$ 25.00 "));

		}
		else if(x == 2 && tipoPorte.contentEquals("grande".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DA TOSA SERA DE R$ 30.00 "));
		}
		else if(x == 2 && tipoPorte.contentEquals("gigante".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DA TOSA SERA DE R$ 35.00 "));
		}
		else if(x == 3 && tipoPorte.contentEquals("pequeno".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DA TOSA E DO BANHO SERA DE R$ 25.00"));
		}
		else if(x == 3 && tipoPorte.contentEquals("medio".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DA TOSA E DO BANHO SERA DE R$ 40.00"));
		}
		else if(x == 3 && tipoPorte.contentEquals("grande".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DA TOSA E DO BANHO SERA DE R$ 50.00"));
		}
		else if(x == 3 && tipoPorte.contentEquals("gigante".toUpperCase())) {
			ServicosSalaoDAO.add(cliente, new ServicosSalao(pet,"O VALOR DA TOSA E DO BANHO SERA DE R$ 60.00"));

		}
		else {
			System.out.println("ENTRADA INVALIDA");
		}
		contador++;
		System.out.println("AINDA DESEJA FAZER ALGUM CADASTRO?! SE SIM,DIGITE [0], SE N�O DIGITE [Digite qualquer outro numero]: ");
		escolha = sc.nextInt();
	}while(escolha == 0);

	}
	public static void listar() {
		/*for (int i = 0; i < ClienteDAO.getList().size(); i++) {
			System.out.println(ClienteDAO.getList().get(i).toString());
			System.out.println(PetDAO.getList().get(i).toString());
			System.out.println(ServicaoSalaoDAO.getListaServicos().entrySet().toString());
		}*/
		
	 for (Entry<Cliente, ServicosSalao> cliente : ServicosSalaoDAO.getListaServicos().entrySet()) {
		System.out.println(cliente.getKey().toString());
		System.out.println(cliente.getValue().toString()+"\n");
	}	
	}	                                                     
}
