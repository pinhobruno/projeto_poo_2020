package br.ucsal.bes2020.poo.ted.TUI;

import java.util.Map.Entry;
import java.util.Scanner;

import br.ucsal.bes2020.poo.ted.domain.Cliente;
import br.ucsal.bes2020.poo.ted.domain.Pet;
import br.ucsal.bes2020.poo.ted.domain.ClinicaVeterinaria;
import br.ucsal.bes2020.poo.ted.persistence.ClienteDAO;
import br.ucsal.bes2020.poo.ted.persistence.PetDAO;
import br.ucsal.bes2020.poo.ted.persistence.VeterinariosDAO;
import br.ucsal.bes2020.poo.ted.domain.Veterinario;
import br.ucsal.bes2020.poo.ted.enums.PorteAnimal;
import br.ucsal.bes2020.poo.ted.enums.Turno;

public class AtendimentoVeterinarioTUI {
	
	public static void Cadastro() {
		int contador = 0;
		int escolha;
		do {
		if(contador > 0 ) {
			System.out.println("VAMOS PARA O "+(contador+1)+"� "+"CADASTRO NA LISTA DO SISTEMA");
		}
		Scanner sc = new Scanner(System.in);
		System.out.println();
		System.out.println("------------------------------------------");
		System.out.println("ENTRE COM OS DADOS DO CLIENTE: ");
		System.out.println("");
		System.out.println("NOME: ");
		String nome = sc.next();
		System.out.println("CPF: ");
		String cpf = sc.next();
		System.out.println("TELEFONE: ");
		String telefone = sc.next();
		System.out.println("E-MAIL: ");
		String email = sc.next();
		System.out.println("CEP: ");
		String cep = sc.next();
		System.out.println();
		Cliente cliente = new Cliente(nome, cpf, telefone, email, cep);
		ClienteDAO.addCliente(cliente);
		System.out.println();
		System.out.println("------------------------------------------");
		System.out.println("ENTRE COM OS DADOS DO PET: ");
		System.out.println("");
		System.out.println("NOME DO PET: ");
		String nomePet = sc.next();
		System.out.println("ESP�CIE: ");
		String especie = sc.next();
		System.out.println("RA�A: ");
		String raca = sc.next();
		System.out.println("TIPO DE PORTE DO ANIMAL : PEQUENO || MEDIO || GRANDE || GIGANTE");
		String tipoPorte = sc.next().toUpperCase();
		System.out.println();
		System.out.println("------------------------------------------");
		Pet pet = new  Pet(nomePet,especie,raca,PorteAnimal.valueOf(tipoPorte));
		PetDAO.addPet(pet);
		
		System.out.println("INFORME O TURNO PARA AGENDAMENTO \nMATUTINO\nVESPERTINO\nNOTURNO");
		String turno = sc.next().toUpperCase();
		System.out.println();
		VeterinariosDAO vete = new VeterinariosDAO();
		/*/while(turno != "MATUTINO" && turno != "VESPERTINO" && turno != "NOTURNO") {
			System.out.println("Digite novamente ");
			turno = sc.next().toUpperCase();
		}/*/
		if (turno.equalsIgnoreCase("matutino")) {
	
			Veterinario veterinario = new Veterinario(pet,"R$ 300.00",Turno.MATUTINO,"Igor","3000");
			
			VeterinariosDAO.addVeterinario(veterinario);
			
			
		}else if (turno.equalsIgnoreCase("vespertino")) {
			Veterinario veterinario = new Veterinario(pet,"R$ 350",Turno.VESPERTINO,"Ian","5234");
			VeterinariosDAO.addVeterinario(veterinario);
		
		}else if (turno.equalsIgnoreCase("noturno")) {
			Veterinario veterinario = new Veterinario(pet,"R$ 400",Turno.NOTURNO,"Pedro","9231");
			VeterinariosDAO.addVeterinario(veterinario);
			
		}
		else {
			System.out.println("Veterinairo nao encontrado");
		}
		//ServicosSalao servicosSalao = new ServicosSalao(pet,retorno);
		
		//if(x == 1 && tipoPorte.contentEquals("pequeno".toUpperCase()))
		
		contador++;
		System.out.println("AINDA DESEJA FAZER ALGUM CADASTRO?! SE SIM,DIGITE [0], SE N�O DIGITE [Digite qualquer outro numero]: ");
		escolha = sc.nextInt();
	}while(escolha == 0);

	}
	public static void listar() {
		for (int i = 0; i < ClienteDAO.getList().size(); i++) {
			System.out.println(ClienteDAO.getList().get(i).toString());
			System.out.println(PetDAO.getList().get(i).toString());
			System.out.println(VeterinariosDAO.getList().get(i).toString());
		}
	
	}	                                                     
}
