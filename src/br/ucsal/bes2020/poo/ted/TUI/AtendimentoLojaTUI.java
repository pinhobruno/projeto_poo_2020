package br.ucsal.bes2020.poo.ted.TUI;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.ucsal.bes2020.poo.ted.domain.Cliente;
import br.ucsal.bes2020.poo.ted.domain.ProdutoAlimento;
import br.ucsal.bes2020.poo.ted.domain.ProdutoHigiene;
import br.ucsal.bes2020.poo.ted.domain.ProdutoLazer;
import br.ucsal.bes2020.poo.ted.enums.IdadeAnimal;
import br.ucsal.bes2020.poo.ted.enums.PesoAnimal;
import br.ucsal.bes2020.poo.ted.enums.PorteAnimal;
import br.ucsal.bes2020.poo.ted.enums.TipoDeAlimento;
import br.ucsal.bes2020.poo.ted.enums.TipoDePele;
import br.ucsal.bes2020.poo.ted.enums.TipoHigiene;
import br.ucsal.bes2020.poo.ted.enums.TipoLazer;
import br.ucsal.bes2020.poo.ted.persistence.ProdutoAlimentoDAO;
import br.ucsal.bes2020.poo.ted.persistence.ProdutoHigieneDAO;
import br.ucsal.bes2020.poo.ted.persistence.ProdutoLazerDAO;

public class AtendimentoLojaTUI {

	public static void Cadastro() {

		int contador = 0;

		Scanner sc = new Scanner(System.in);
		System.out.println("BEM-VINDO(A) A NOSSA LOJA !!");
		System.out.println("");
		System.out.println("-----TIPO DE PRODUTO A SER ESCOLHIDO-----");
		System.out.println();
		System.out.println("***********************************");
		System.out.println("[1]- ALIMENTAÇAO ");
		System.out.println("***********************************");
		System.out.println("[2]- HIGIENE ");
		System.out.println("***********************************");
		System.out.println("[3]- LAZER ");
		System.out.println("***********************************");

		System.out.println();

		System.out.println("O CLIENTE DESEJA O SERVICO DO TIPO : ");
		int x = sc.nextInt();
		String nota = "";
		switch (x) {
		case 1:

			System.out.println("------------------------------------------");
			System.out.println("ENTRE COM AS ESPECIFICACOES PARA O PRODUTO ALIMENTICIO: ");
			System.out.println("");
			System.out.println("TIPO ALIMENTO : [RACAO] || [OSSO] || [PETISCO]");
			String tipoAlimento = sc.next();

			System.out.println("PORTE DO ANIMAL : [PEQUENO] || [MEDIO] || [GRANDE] || [GIGANTE]");

			String porteAnimal = sc.next();

			System.out.println("IDADE DO ANIMAL : [FILHOTE] || [JOVEM] || [ADULTO] || [IDOSO]");

			String idadeAnimal = sc.next();

			System.out.println("PESO DO ANIMAL : [CAQUETICO] || [MAGRO] || [NORMAL] || [SOBREPESO] || [OBESO]");

			String pesoAnimal = sc.next();

			ProdutoAlimentoDAO.addEstoque();

			ProdutoAlimento produtoAlimento = new ProdutoAlimento(PorteAnimal.valueOf(porteAnimal.toUpperCase()),
					IdadeAnimal.valueOf(idadeAnimal.toUpperCase()), PesoAnimal.valueOf(pesoAnimal.toUpperCase()),
					TipoDeAlimento.valueOf(tipoAlimento.toUpperCase()));

			for (ProdutoAlimento e : ProdutoAlimentoDAO.getList()) {
				if (produtoAlimento.equals(e)) {
					System.out.println(
							"TOTAL DE ITENS DE ALIMENTACAO EM ESTOQUE === " + ProdutoAlimentoDAO.totalEstoque());
					System.out.println();
					System.out.println();

					System.out.println(
							"O produto solicitado foi (======> ".toUpperCase() + e.getNomeProdutos().toUpperCase()
									+ "com o custo de R$".toUpperCase() + e.getValorProdutos());

					System.out.println();
					System.out.println();
					// ProdutoAlimentoDAO.removerItemHigiene(e);
					System.out.println();
					System.out.println();
					System.out.println();

					nota += "                                       NOTA                                 \n"
							+ "============================================================================================="
							+ "\n" + "Nome do Produto = " + e.getNomeProdutos() + "||| Preco do Produto = "
							+ e.getValorProdutos() + "\n"
							+ "=============================================================================================";

					System.out.println(nota);

					System.out.println();

					System.out.println(" VOLTE SEMPRE !!");

					System.out.println();

				}
			}

			break;

		case 2:

			System.out.println("------------------------------------------");
			System.out.println("ENTRE COM AS ESPECIFICACOES PARA O PRODUTO DE HIGIENE: ");
			System.out.println("");
			System.out.println("TIPO HIGIENE : [SHAMPOO] || [SABAO] || [ESCOVAS]");
			String tipoHigiene = sc.next();

			System.out.println("TIPO DA PELE : [NORMAL] || [SENSIVEL] |");

			String tipoPele = sc.next();

			ProdutoHigieneDAO.addEstoque();

			ProdutoHigiene produtoHigiene = new ProdutoHigiene(TipoHigiene.valueOf(tipoHigiene.toUpperCase()),
					TipoDePele.valueOf(tipoPele.toUpperCase()));

			for (ProdutoHigiene e :ProdutoHigieneDAO.getList()) {
				if (produtoHigiene.equals(e)) {
					System.out.println("TOTAL DE ITENS DE DE LAZER EM ESTOQUE === " + ProdutoHigieneDAO.totalEstoque());
					System.out.println();
					System.out.println();

					System.out.println(
							"O produto solicitado foi (======> ".toUpperCase() + e.getNomeProdutos().toUpperCase()
									+ " com o custo de R$".toUpperCase() + e.getValorProdutos());

					System.out.println();
					System.out.println();

					System.out.println();
					System.out.println();
					System.out.println();

					nota += "                                       NOTA                                 \n"
							+ "============================================================================================="
							+ "\n" + "Nome do Produto = " + e.getNomeProdutos() + "||| Preco do Produto = "
							+ e.getValorProdutos() + "\n"
							+ "=============================================================================================";

					System.out.println(nota);

					System.out.println();

					System.out.println(" VOLTE SEMPRE !!");

					System.out.println();
					break;
				}

			}
			break;

		case 3:
			System.out.println("------------------------------------------");
			System.out.println("ENTRE COM AS ESPECIFICACOES PARA O PRODUTO DE LAZER: ");
			System.out.println("");
			System.out.println("TIPO LAZER : [HABITACAO] || [CAMA] || [BRINQUEDO]");
			String tipoLazer = sc.next();

			System.out.println("PORTE DO ANIMAL : [PEQUENO] || [MEDIO] || [GRANDE] || [GIGANTE]");

			String porteAnimal02 = sc.next();

			ProdutoLazerDAO.addEstoque();

			ProdutoLazer produtoLazer = new ProdutoLazer(TipoLazer.valueOf(tipoLazer.toUpperCase()),
					PorteAnimal.valueOf(porteAnimal02.toUpperCase()));

			for (ProdutoLazer e : ProdutoLazerDAO.getList()) {
				if (produtoLazer.equals(e)) {
					System.out.println("TOTAL DE ITENS DE DE LAZER EM ESTOQUE === " + ProdutoLazerDAO.totalEstoque());
					System.out.println();
					System.out.println();

					System.out.println(
							"O produto solicitado foi (======> ".toUpperCase() + e.getNomeProdutos().toUpperCase()
									+ " com o custo de R$".toUpperCase() + e.getValorProdutos());

					System.out.println();
					System.out.println();

					System.out.println();
					System.out.println();
					System.out.println();

					nota += "                                       NOTA                                 \n"
							+ "============================================================================================="
							+ "\n" + "Nome do Produto = " + e.getNomeProdutos() + "||| Preco do Produto = "
							+ e.getValorProdutos() + "\n"
							+ "=============================================================================================";

					System.out.println(nota);

					System.out.println();

					System.out.println(" VOLTE SEMPRE !!");

					System.out.println();

				}

			}

			break;

		default:

			System.out.println("Opcao Invalida".toUpperCase());

		}

		contador++;

	}
}
