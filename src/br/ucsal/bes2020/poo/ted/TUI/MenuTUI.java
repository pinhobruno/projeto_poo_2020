package br.ucsal.bes2020.poo.ted.TUI;

import java.util.InputMismatchException;
import java.util.Scanner;



public class MenuTUI {
	public static String[] obterNomes() {
		String[] nomes = { "mario", "jose", "neiva" };
		return nomes;
	}

	// VETOR PARA PEGAS AS SENHAS
	public static int[] obterSenhas() {
		int[] senhas = { 123, 1234, 12345 };
		return senhas;
	}

	// VETOR PARA AUTENTICAR O NOME E A SENHA
	public static String obterAutenticcao(String[] nome, int[] senhas) {
		Scanner sc = new Scanner(System.in);
		int contador = 0;
		imprimir("                 Bem-vindo(a) ao PetShop                ".toUpperCase());
		imprimir("");
		imprimir("Informe seu usuario de entrada: ".toUpperCase());
		String nome_usuario = sc.nextLine();
		imprimir("Informe sua senha: ".toUpperCase());
		int senha_usuario = 0;
		boolean erro = false;
		
		
		try {
			senha_usuario = sc.nextInt();
		} 
		catch(InputMismatchException e) {
			System.out.println("FOI INFORMADO UM CARACTER  ONDE DEVIA ENTRAR NUMERO");
			erro = true;
		}
		catch (Exception e) {
			System.out.println("NAO FOI POSSIVEL REALIZAR A ENTRADA !!");
			erro = true;
		}
		String retorno = ("USU�RIO ERRADO!");
		if(!erro) {
			
		
		
		for (int i = 0; i < nome.length; i++) {
			if (nome_usuario.equals(nome[i]) && senha_usuario == senhas[i]) {
				retorno = "Bem-vindo(a), funcion�rio(a) " + nome[i].toUpperCase() + " !!";
				i = nome.length;
			} else if (nome_usuario.equals(nome[i]) && senha_usuario != senhas[i]) {
				do {
					imprimir("Insira novamente sua senha. Voc� tem mais ".toUpperCase() + (4 - contador) + " chances.".toUpperCase());
					
					senha_usuario = sc.nextInt();

					contador++;

					if (senha_usuario == senhas[i]) {

						retorno = "Bem-vindo(a), funcion�rio(a) ".toUpperCase() + nome[i].toUpperCase() + " !!".toUpperCase();
					}
					if (contador == 4) {
						imprimir("Voce nao � um funcionrio do PetShop !! Contate um Admninistrador !! \n".toUpperCase());

						System.exit(0);

					}
				} while (senha_usuario != senhas[i]);
			}

		}
		}
		return retorno;
	}
	

	// METODO PARA SUBSTITUIR O SYSOUT
	public static void imprimir(String txt) {
		System.out.println(txt);
	}

	// METODO PARA SELECIONAR O TIPO
	public static void switchMenu() {
		Scanner sc = new Scanner(System.in);
		imprimir("");
		//ProdutoVestuarioDAO.addEstoque();
		//ProdutoVestuarioDAO.listar();
		imprimir("[1]-SAL�O  || [2] - PET LOJA || [3] - VETERINARIO || [4]- SAIR  ");
		int escolha = sc.nextInt();
		switch(escolha) {
		case 1:{
			imprimir("");
			imprimir("***********************************");
			imprimir("VAMOS CADASTRAR O CLIENTE PRIMEIRO: ");
			imprimir("***********************************");
			AtendimentoSalaoTUI.Cadastro();
			AtendimentoSalaoTUI.listar();
			break;
		}
		case 2:
			imprimir("");
			imprimir("***********************************");
			imprimir("VAMOS ENTRAR NA LOJA: ");
			imprimir("***********************************");
			AtendimentoLojaTUI.Cadastro();
			

			break;
		case 3:
			imprimir("");
			imprimir("***********************************");
			imprimir("VAMOS CADASTRAR O CLIENTE PRIMEIRO: ");
			imprimir("***********************************");
			AtendimentoVeterinarioTUI.Cadastro();
			AtendimentoVeterinarioTUI.listar();
			break;
		case 4:
			imprimir("");
			imprimir("Volte-Sempre");
			System.exit(0);
			break;
		default:
			imprimir("Op��o inv�lida !!");
			System.exit(0);
		}
	}
}