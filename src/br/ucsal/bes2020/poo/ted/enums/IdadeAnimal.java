package br.ucsal.bes2020.poo.ted.enums;

public enum IdadeAnimal {
	
	FILHOTE, JOVEM, ADULTO, IDOSO;
}
