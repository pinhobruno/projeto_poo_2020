package br.ucsal.bes2020.poo.ted.enums;

public enum PesoAnimal {
	
	CAQUETICO, MAGRO, NORMAL, SOBREPESO, OBESO;

}
