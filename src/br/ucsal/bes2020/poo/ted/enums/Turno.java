package br.ucsal.bes2020.poo.ted.enums;

public enum Turno {
	MATUTINO, VESPERTINO, NOTURNO;
}
