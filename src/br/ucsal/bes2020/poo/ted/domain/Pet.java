package br.ucsal.bes2020.poo.ted.domain;

import br.ucsal.bes2020.poo.ted.enums.PorteAnimal;

public class Pet {
    private String nome;
    private String especie;
    private String raca;
    private PorteAnimal porteAnimal;

    public Pet(String nome, String especie, String raca, PorteAnimal porteAnimal) {
		super();
		this.nome = nome;
		this.especie = especie;
		this.raca = raca;
		this.porteAnimal = porteAnimal;
	}

    
	public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public PorteAnimal getPorteAnimal() {
        return porteAnimal;
    }

    public void setPorteAnimal(PorteAnimal porteAnimal) {
        this.porteAnimal = porteAnimal;
    }

	@Override
	public String toString() {
		return "[ NOME DO ANIMAL: "+nome.toUpperCase()+"] | " + "[ ESPECIE : "+especie.toUpperCase()+"] | " +" [ RA�A : " + raca.toUpperCase()+"] | " + " [ PORTE DO ANIMAL :  " + porteAnimal + "] ||";
	}

    


}