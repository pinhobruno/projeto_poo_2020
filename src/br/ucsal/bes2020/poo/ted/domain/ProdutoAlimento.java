package br.ucsal.bes2020.poo.ted.domain;

import br.ucsal.bes2020.poo.ted.enums.IdadeAnimal;
import br.ucsal.bes2020.poo.ted.enums.PesoAnimal;
import br.ucsal.bes2020.poo.ted.enums.PorteAnimal;
import br.ucsal.bes2020.poo.ted.enums.TipoDeAlimento;

public class ProdutoAlimento extends Produto {

	/*
	 * Itens de Alimentos: Os argumentos utilizados ser�o dividos por ENUM(Peso,
	 * idade, tipo e tamanho), sendo que para: Tamanho (Pequeno, M�dio, Grande e
	 * Gigante) e Peso(caqu�tico, magro, normal, sobrepeso e obeso)
	 */

	private PorteAnimal tamanho;
	private IdadeAnimal idade;
	private PesoAnimal peso;
	private TipoDeAlimento alimento;

	public ProdutoAlimento(String nomeProdutos, double valorProdutos, PorteAnimal tamanho, IdadeAnimal idade,
			PesoAnimal peso, TipoDeAlimento alimento) {
		super(nomeProdutos, valorProdutos);
		this.tamanho = tamanho;
		this.idade = idade;
		this.peso = peso;
		this.alimento = alimento;
	}

	public ProdutoAlimento(PorteAnimal tamanho, IdadeAnimal idade, PesoAnimal peso, TipoDeAlimento alimento) {

		this.tamanho = tamanho;
		this.idade = idade;
		this.peso = peso;
		this.alimento = alimento;

	}

	public PorteAnimal getTamanho() {
		return tamanho;
	}

	public void setTamanho(PorteAnimal tamanho) {
		this.tamanho = tamanho;
	}

	public IdadeAnimal getIdade() {
		return idade;
	}

	public void setIdade(IdadeAnimal idade) {
		this.idade = idade;
	}

	public PesoAnimal getPeso() {
		return peso;
	}

	public void setPeso(PesoAnimal peso) {
		this.peso = peso;
	}

	public TipoDeAlimento getAlimento() {
		return alimento;
	}

	public void setAlimento(TipoDeAlimento alimento) {
		this.alimento = alimento;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoAlimento other = (ProdutoAlimento) obj;
		if (alimento != other.alimento)
			return false;
		if (idade != other.idade)
			return false;
		if (peso != other.peso)
			return false;
		if (tamanho != other.tamanho)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProdutoAlimento [tamanho = " + tamanho + ", idade = " + idade + ", peso = " + peso + ", alimento = "
				+ alimento + "]";
	}

}
