package br.ucsal.bes2020.poo.ted.domain;

import java.util.List;
import java.util.ArrayList;

public class Cliente {

    private String nome;
    private String cpf;
    private String telefone;
    private String email;
    private String cep;
    
    public Cliente(String nome, String cpf, String telefone, String email, String cep) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.email = email;
		this.cep = cep;
	}
   
    
    public String getCep() {
		return cep;
	}


	public void setCep(String cep) {
		this.cep = cep;
	}


	public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


	@Override
	public String toString() {
		return "|| INFO CLIENTE : "+"[ NOME : "+ nome.toUpperCase()+"] | " + "[ CPF : " + cpf.toUpperCase() + "] | [ TELEFONE: " + telefone.toUpperCase() + "] | [ EMAIL: " + email.toUpperCase() + "] | [ CEP: " + cep.toUpperCase()
				+ "] || ";
	}

  
}

