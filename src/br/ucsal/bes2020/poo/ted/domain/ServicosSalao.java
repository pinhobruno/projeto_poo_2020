package br.ucsal.bes2020.poo.ted.domain;

public class ServicosSalao {

	private Pet pet;
	private String valor;
	

	public ServicosSalao(Pet pet, String valor) {
		super();
		this.pet = pet;
		this.valor = valor;
	}
	
	public Pet getPet() {
		return pet;
	}
	public void setPet(Pet pet) {
		this.pet = pet;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "|| SERVI�O FEITO : " +valor+" NO  PET : " +pet + " ";
	}
	
}
