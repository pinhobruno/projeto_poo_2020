package br.ucsal.bes2020.poo.ted.domain;

public class Loja {
	
	private String nomeProdutos;
	private double valorProdutos;
	private Produto produto;

	
	public Loja(String nomeProdutos, double valorProdutos, ProdutoAlimento produto) {
		super();
		this.nomeProdutos = nomeProdutos;
		this.valorProdutos = valorProdutos;
		this.produto = produto;
	}
	
	public Loja(String nomeProdutos, double valorProdutos, ProdutoHigiene produto) {
		super();
		this.nomeProdutos = nomeProdutos;
		this.valorProdutos = valorProdutos;
		this.produto = produto;
	}
	public Loja(String nomeProdutos, double valorProdutos, ProdutoLazer produto) {
		super();
		this.nomeProdutos = nomeProdutos;
		this.valorProdutos = valorProdutos;
		this.produto = produto;
	}
	public String getNomeProdutos() {
		return nomeProdutos;
	}
	public void setNomeProdutos(String nomeProdutos) {
		this.nomeProdutos = nomeProdutos;
	}
	public double getValorProdutos() {
		return valorProdutos;
	}
	public void setValorProdutos(double valorProdutos) {
		this.valorProdutos = valorProdutos;
	}
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	@Override
	public String toString() {
		return "Loja [nomeProdutos=" + nomeProdutos + ", valorProdutos=" + valorProdutos + ", produto=" + produto + "]";
	}


}
