package br.ucsal.bes2020.poo.ted.domain;

import br.ucsal.bes2020.poo.ted.enums.Turno;

public class Veterinario extends ClinicaVeterinaria  {
	
	private String nome;
	private String crmv;
	

	public Veterinario(Pet pet, String valor, Turno turno, String nome, String crmv) {
		super(pet, valor, turno);
		this.nome = nome;
		this.crmv = crmv;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getCrmv() {
		return crmv;
	}


	public void setCrmv(String crmv) {
		this.crmv = crmv;
	}


	@Override
	public String toString() {
		return "|| INFO VETERINARIO [NOME = " + nome + ", CRMv = " + crmv + ", VALOR DA CONSULTA R$ "+getValor();
	}
	
	




	

	
	
	

	
	
	
	
}
