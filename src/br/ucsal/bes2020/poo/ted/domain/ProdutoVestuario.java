package br.ucsal.bes2020.poo.ted.domain;

import br.ucsal.bes2020.poo.ted.enums.TipoVestuario;

public class ProdutoVestuario extends Produto {
	
	// Implementa��o de um array-list para cada item de Vestu�rio: Coleiras, Roupas e Presilhas

	private String tamanho;
	private TipoVestuario tipo;
	
	public ProdutoVestuario(String nomeProdutos, double valorProdutos, String tamanho, TipoVestuario tipo) {
		super(nomeProdutos, valorProdutos);
		this.tamanho = tamanho;
		this.tipo = tipo;
	}

	public String getTamanho() {
		return tamanho;
	}

	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	public TipoVestuario getTipo() {
		return tipo;
	}

	public void setTipo(TipoVestuario tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "ProdutoVestuario [nome=" + getNomeProdutos() + ",valor="+getValorProdutos()+" tipo=" + tipo + "]";
	}

}
