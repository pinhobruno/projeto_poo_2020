package br.ucsal.bes2020.poo.ted.domain;

import br.ucsal.bes2020.poo.ted.enums.PorteAnimal;
import br.ucsal.bes2020.poo.ted.enums.TipoLazer;

public class ProdutoLazer extends Produto {
	
	/*Itens de Lazer: Ser� implementado um ENUMs para tipo: Habita��o, cama e brinquedo.
	 *  E outro ENUM para relcacionar de acordo com o tamanho do PET. Em Desenvolvimento
	 */
	
	private TipoLazer lazer;
	private PorteAnimal tamanho;
	
	public ProdutoLazer(String nomeProdutos, double valorProdutos,TipoLazer lazer,PorteAnimal tamanho) {
		super(nomeProdutos, valorProdutos);
		this.lazer = lazer;
		this.tamanho = tamanho;
	}
	
	public ProdutoLazer(TipoLazer lazer,PorteAnimal tamanho) {
		this.lazer = lazer;
		this.tamanho = tamanho;
	}

	public TipoLazer getLazer() {
		return lazer;
	}

	public void setLazer(TipoLazer lazer) {
		this.lazer = lazer;
	}

	public PorteAnimal getTamanho() {
		return tamanho;
	}

	public void setTamanho(PorteAnimal tamanho) {
		this.tamanho = tamanho;
	}
/*
public boolean equals(ProdutoAlimento e) {
		
		
		return this.getLazer().equals(e.getAlimento()) 
				&& this.getLazer().equals(e.getTamanho()) 
				&& this.getLazer().equals(e.getIdade())
				&& this.getPeso().equals(e.getPeso())
				;
		
	}*/
	
	@Override
	public String toString() {
		return "ProdutoLazer [lazer=" + lazer + ", tamanho=" + tamanho + "]";
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoLazer other = (ProdutoLazer) obj;
		if (lazer != other.lazer)
			return false;
		if (tamanho != other.tamanho)
			return false;
		return true;
	}
	
}
