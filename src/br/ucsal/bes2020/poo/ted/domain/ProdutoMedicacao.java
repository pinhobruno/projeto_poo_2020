package br.ucsal.bes2020.poo.ted.domain;

import br.ucsal.bes2020.poo.ted.enums.TiposMedicacoes;

public class ProdutoMedicacao extends Produto {
	/*Itens de Medica��es: Ser�o implementados no ENUM (tipos): Antibi�tico,
	 * Anti-Viral, Anti-Inflamat�rio e Verm�fugo. (double gramagem: 0,25g, 0,50g, 0,75g, 1,00g)
	 * Em Desenvolvimento
	 */
	
	private TiposMedicacoes medicacoes;
	private double gramagem;
	
	public ProdutoMedicacao(String nomeProdutos, double valorProdutos,TiposMedicacoes medicacoes, double gramagem) {
		super(nomeProdutos, valorProdutos);
		this.medicacoes = medicacoes;
		this.gramagem = gramagem;
	}

	public TiposMedicacoes getMedicacoes() {
		return medicacoes;
	}

	public void setMedicacoes(TiposMedicacoes medicacoes) {
		this.medicacoes = medicacoes;
	}

	public double getGramagem() {
		return gramagem;
	}

	public void setGramagem(double gramagem) {
		this.gramagem = gramagem;
	}

	@Override
	public String toString() {
		return "ProdutoMedicacao [medicacoes=" + medicacoes + ", gramagem=" + gramagem + "]";
	}
	
}
