package br.ucsal.bes2020.poo.ted.domain;

import br.ucsal.bes2020.poo.ted.enums.IdadeAnimal;
import br.ucsal.bes2020.poo.ted.enums.PesoAnimal;
import br.ucsal.bes2020.poo.ted.enums.PorteAnimal;
import br.ucsal.bes2020.poo.ted.enums.TipoDeAlimento;
import br.ucsal.bes2020.poo.ted.enums.TipoDePele;
import br.ucsal.bes2020.poo.ted.enums.TipoHigiene;

public class ProdutoHigiene extends Produto {

//Itens de Higiene:Ser�o implementados ENUM para os itens: Shampoo,
//sab�o e escovas e Implementado outro ENUM para os tipos de pele: Normal ou Sens�vel. 

	private TipoHigiene higiene;
	private TipoDePele pele;

	public ProdutoHigiene(String nomeProdutos, double valorProdutos, TipoHigiene higiene, TipoDePele pele) {
		super(nomeProdutos, valorProdutos);
		this.higiene = higiene;
		this.pele = pele;
	}

	public ProdutoHigiene(TipoHigiene higiene, TipoDePele pele) {
		
		this.higiene = higiene;
		this.pele = pele;

	}

	public TipoHigiene getHigiene() {
		return higiene;
	}

	public void setHigiene(TipoHigiene higiene) {
		this.higiene = higiene;
	}

	public TipoDePele getPele() {
		return pele;
	}

	public void setPele(TipoDePele pele) {
		this.pele = pele;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoHigiene other = (ProdutoHigiene) obj;
		if (higiene != other.higiene)
			return false;
		if (pele != other.pele)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProdutoHigiene [higiene=" + higiene + ", pele=" + pele + "]";
	}

}
