package br.ucsal.bes2020.poo.ted.domain;

import br.ucsal.bes2020.poo.ted.enums.Turno;

public class ClinicaVeterinaria {
	//Servi�os da Clinica 
	
	private Pet pet;
	private String valor;
	private Turno turno;
	
	public ClinicaVeterinaria(Pet pet, String valor, Turno turno) {
		super();
		this.pet = pet;
		this.valor = valor;
		this.turno = turno;
	}

	public Pet getPet() {
		return pet;
	}
	public void setPet(Pet pet) {
		this.pet = pet;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

}
