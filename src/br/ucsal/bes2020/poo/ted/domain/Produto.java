package br.ucsal.bes2020.poo.ted.domain;

public class Produto {

	
	private String nomeProdutos;
	private double valorProdutos;

	//HIGIENE,VESTUARIO,ALIMENTACAO,MEDICACAO,LAZER
	
	
	
	public Produto(String nomeProdutos, double valorProdutos) {
		super();
		this.nomeProdutos = nomeProdutos;
		this.valorProdutos = valorProdutos;
	}
	
	
	public Produto() {
		super();
	}
	public String getNomeProdutos() {
		return nomeProdutos;
	}
	public void setNomeProdutos(String nomeProdutos) {
		this.nomeProdutos = nomeProdutos;
	}
	public double getValorProdutos() {
		return valorProdutos;
	}
	public void setValorProdutos(double valorProdutos) {
		this.valorProdutos = valorProdutos;
	}

	@Override
	public String toString() {
		return "Loja [nomeProdutos=" + nomeProdutos + ", valorProdutos=" + valorProdutos;
	}


}
